import java.util.Scanner;
public class AdditionOfTwoNumbers {

    public static String inputString(){
        Scanner scanner = new Scanner(System.in);
        String string = scanner.next();

        return string;
    }

    public static long stringToLong(){

        String stringToConvert = inputString();
        long number = Long.valueOf(stringToConvert);

        return number;
    }
}
