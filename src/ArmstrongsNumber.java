import java.util.Scanner;

public class ArmstrongsNumber {

    public static int inputMethod(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number: ");
        int k = scanner.nextInt();
        return k;
    }

    public static int noOfDigits(int num) {
        int i;
        for (i = 0; num > 0; i++) {
            num /= 10;
        }
        return i;
    }

    public static int requiredSum(int num) {
        int i = noOfDigits(num);
        int sum = 0;
        while (num > 0) {
            int digit = num % 10;
            num /= 10;
            sum += Math.pow(digit, i);
        }
        return sum;
    }
}
