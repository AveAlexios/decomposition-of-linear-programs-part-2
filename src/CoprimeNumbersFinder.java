import java.util.Scanner;
public class CoprimeNumbersFinder {

    /* You input 3 positive integers in method Input Array. Values are validated there
     with help of the other method 'Validating Input',
      and are passed back into array, which InputArray method creates.

    Then the values from Input Array are passed to the greatestCommonDivisor via main method
    And the greatest common divisor of those three defines what will be outputted.
    */
    public static int greatestCommonDivisorOfThree(int ... array){
        int x = array[0];
        int y = array[1];
        int z = array[2];
        int commonDivisor = 1;
        int minimal = Math.min(z, Math.min(x,y) );
        for(int i = 1; i <= minimal; i++){
            if((x % i == 0) && (y % i == 0) && (z % i == 0)) {
                commonDivisor = i;
            }
        }return commonDivisor;
    }

    public static int[] inputArray(){
        int[] array = new int[3];

        for(int i= 0; i < 3; i++) {
            array[i] = validatingInput();

        }return array;
    }

    public static int validatingInput() {
        Scanner scanner = new Scanner(System.in);
        int number;
        do {
            System.out.print("Please enter an Integer: ");
            while (!scanner.hasNextInt()) {
                String input = scanner.next();
                System.out.printf("This is not an Integer: ", input);
            }
            number = scanner.nextInt();
        } while (number < 0);
        System.out.printf("You have entered a number : %d.%n", number);
        return number;
    }


}
