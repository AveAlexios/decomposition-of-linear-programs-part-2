/*
 This program consists of three classes - according to the three tasks from the module.
 To execute each of the methods just open the comment brackets and run the program.
*/

public class Main {

    public static void main(String[] args){

// Code Part for task3 - Armstrong Number

        int k = ArmstrongsNumber.inputMethod();

        for(int i = 0; i <= k; i++) {
            int sum = ArmstrongsNumber.requiredSum(i);

            if (i == sum) {
                System.out.print(i + " ");
            }
        }
    }
}



/*      Code part for task2 - Addition of two Numbers

       long numberFirst = AdditionOfTwoNumbers.stringToLong();
       long numberSecond = AdditionOfTwoNumbers.stringToLong();
       long sum = numberFirst + numberSecond;

        System.out.println("Sum of your numbers is: " + sum);
*/



/*          Code part for Coprime numbers aka Task 1:

        int[] array = CoprimeNumbersFinder.inputArray();
        int commonDivisor = CoprimeNumbersFinder.greatestCommonDivisorOfThree(array);

        System.out.println();

        if(commonDivisor == 1){
            System.out.println("Yasss, they are definitely coprime");

        }else{
            System.out.println("No, they are not coprime :(");
        }
*/

